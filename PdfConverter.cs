using System.IO;
using System.Linq;
using Aspose.Pdf;
using Aspose.Pdf.Forms;
using Aspose.Pdf.Text;

namespace Aspose.Levi.Example
{
  public static class PdfConverter
  {
    public static void Main(string[] args)
    {
      var inputFile = args.First();
      var outputFile = $"{inputFile}_flattened.pdf";

      // required to not blow up with the FontCollection error
      FontRepository.Sources.Add(new FolderFontSource($"{Directory.GetCurrentDirectory()}/fonts"));
      
      var document = new Document(inputFile);
      document.Form.Type = FormType.Standard;
      document.Save(outputFile);
    }
  }
}
