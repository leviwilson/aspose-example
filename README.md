# aspose-example
This project shows some of the issues I've ran into trying to convert a Dynamic XFA PDF document into a standard AcroForm document. Please see the following thread for more information:

https://forum.aspose.com/t/aspose-pdf-aws-lambda-c-runtime/221326/6


## building
```
docker build -t aspose-levi-example . && docker run --rm -it aspose-levi-example
```
