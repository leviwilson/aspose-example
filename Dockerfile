FROM lambci/lambda:20201112-build-dotnetcore3.1

# download / extrac the libgdiplus and msttffonts layers for the lambda
RUN curl https://levi-share.s3.amazonaws.com/libgdiplus.zip --output libgdiplus.zip && \
    curl https://levi-share.s3.amazonaws.com/msttffonts.zip --output msttffonts.zip && \
    unzip libgdiplus.zip -d libgdiplus && unzip msttffonts.zip

ENTRYPOINT /bin/bash

COPY aspose-example.csproj .
RUN dotnet restore

RUN yum install -y vim

# this just happened to be what was in my layer...symlinking so fonts.conf works
RUN ln -s /var/task/fonts /opt/fonts

# without this it would fail loading libgdiplus
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/var/task/libgdiplus/lib

# expose things like fc-list / fc-cache
ENV PATH=$PATH:/var/task/libgdiplus/bin

# set fontconfig to load fonts from the /var/task/fonts directory that has msttffonts layer
ENV FONTCONFIG_PATH=/var/task/fonts

COPY startup.sh /usr/bin/startup
RUN chmod +x /usr/bin/startup

COPY xfa_form.pdf .
COPY PdfConverter.cs .

ENTRYPOINT ["startup"]
