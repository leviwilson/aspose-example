#! /bin/bash

dotnet run xfa_form.pdf || \
  printf "\n\n^ look...says there are no system fonts\n\nHit any key:" && \
  read -n 1 && printf "\n\n" && fc-list && \
  printf "\n\n^ look...fonts seem to be okay\n\ngive it a go!\n\n" && \
  /bin/bash
